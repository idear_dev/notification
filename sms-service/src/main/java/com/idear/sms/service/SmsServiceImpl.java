/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.PageQueryUtil;
import com.idear.common.util.StringUtil;
import com.idear.common.util.ValidateUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.sms.core.constants.CacheConstant;
import com.idear.sms.core.mapper.SmsHistoryMapper;
import com.idear.sms.core.mapper.SmsTemplateMapper;
import com.idear.sms.core.po.SmsHistory;
import com.idear.sms.core.po.SmsTemplate;
import com.idear.sms.helper.SendMessageHelper;
import com.idear.sms.service.dto.HistoryDto;
import com.idear.sms.service.dto.HistoryListDto;
import com.idear.sms.service.message.SmsMessage;
import com.idear.sms.service.facade.SmsService;
import com.idear.sms.service.param.QueryHistoryParam;
import com.idear.sms.service.param.SendSingleMessageParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author CcShan
 * @date 2017/12/14
 */
@Service("smsService")
public class SmsServiceImpl implements SmsService {

    @Autowired
    private BusinessTemplate businessTemplate;

    @Autowired
    private SendMessageHelper sendMessageHelper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Resource
    private SmsTemplateMapper smsTemplateMapper;

    @Resource
    private SmsHistoryMapper smsHistoryMapper;

    @Override
    public Response sendSingleMessage(final SendSingleMessageParam sendSingleMessageParam) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                //验证参数是否符合格式要求
                ValidateUtil.validate(sendSingleMessageParam);
                //TODO 暂不考虑延迟发送的情况
                String templateCode = sendSingleMessageParam.getTemplateCode();
                SmsTemplate smsTemplate = getTemplate(templateCode);
                if (smsTemplate == null) {
                    throw new BusinessException(SmsMessage.SMS_TEMPLATE_NOT_EXIST);
                }
                String mobileNo = sendSingleMessageParam.getMobileNo();
                String[] params = sendSingleMessageParam.getParams();
                sendMessageHelper.sendSingle(mobileNo, smsTemplate, params);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 查询短信发送记录
     *
     * @param param
     * @return
     */
    @Override
    public Response<HistoryListDto> queryHistory(QueryHistoryParam param) {
        Map<String, Object> paramMap = new HashMap<>(8);
        Integer pageNo, pageSize;
        if (param.getPageNo() == null) {
            pageNo = PageQueryUtil.DEFAULT_PAGE_NO;
        }else {
            pageNo = param.getPageNo();
        }
        if (param.getPageSize() == null || param.getPageSize() > PageQueryUtil.DEFAULT_MAX_PAGE_SIZE) {
            pageSize = PageQueryUtil.DEFAULT_PAGE_SIZE;
        }else {
            pageSize = param.getPageSize();
        }
        if (StringUtil.isNotEmpty(param.getMobileNo())){
            paramMap.put("mobileNo",param.getMobileNo());
        }
        if (StringUtil.isNotEmpty(param.getTemplateCode())){
            paramMap.put("templateCode",param.getTemplateCode());
        }
        if (param.getBeginTime() != null){
            paramMap.put("beginTime",param.getBeginTime());
        }
        if (param.getEndTime() != null){
            paramMap.put("endTime",param.getEndTime());
        }
        PageBounds pageBounds = new PageBounds(pageNo, pageSize);
        PageList<SmsHistory> pageList = smsHistoryMapper.listHistory(paramMap, pageBounds);
        if (pageList != null || !pageList.isEmpty()) {
            HistoryListDto historyListDto = new HistoryListDto();
            Integer totalNum = pageList.getPaginator().getTotalCount();
            Integer totalPage = pageList.getPaginator().getTotalPages();
            historyListDto.setTotalNum(totalNum);
            historyListDto.setTotalPage(totalPage);
            historyListDto.setHistoryList(convert2DtoList(pageList));
            return new Response(historyListDto);
        } else {
            return Response.SUCCESS;
        }
    }

    /**
     * 按照code获取模板信息
     *
     * @param code
     * @return
     */
    public SmsTemplate getTemplate(String code) {
        String template = redisTemplate.opsForValue().get(CacheConstant.SMS_TEMPLATE_INDEX + code);
        if (StringUtil.isEmpty(template)) {
            SmsTemplate smsTemplate = smsTemplateMapper.getSmsTemplateByCode(code);
            if (smsTemplate != null) {
                redisTemplate.opsForValue().set(CacheConstant.SMS_TEMPLATE_INDEX + code, JsonUtil.toJsonString(smsTemplate));
            }
            return smsTemplate;
        } else {
            return JsonUtil.parseObject(template, SmsTemplate.class);
        }
    }

    private HistoryDto convert2Dto(SmsHistory smsHistory) {
        HistoryDto historyDto = new HistoryDto();
        historyDto.setId(smsHistory.getId());
        historyDto.setMobileNo(smsHistory.getMobileNo());
        historyDto.setSerialNo(smsHistory.getSerialNo());
        historyDto.setStatus(smsHistory.getStatus());
        historyDto.setTemplateCode(smsHistory.getTemplateCode());
        historyDto.setThdSerialNo(smsHistory.getThdSerialNo());
        historyDto.setContent(smsHistory.getContent());
        historyDto.setCreateTime(smsHistory.getCreateTime());
        historyDto.setUpdateTime(smsHistory.getUpdateTime());
        return historyDto;
    }

    private List<HistoryDto> convert2DtoList(List<SmsHistory> historyList) {
        List<HistoryDto> dtoList = new ArrayList<>();
        if (historyList != null && !historyList.isEmpty()) {
            Iterator<SmsHistory> iterator = historyList.iterator();
            while (iterator.hasNext()) {
                SmsHistory history = iterator.next();
                dtoList.add(convert2Dto(history));
            }
        }
        return dtoList;
    }
}
