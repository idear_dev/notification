/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.sms.service;

import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.StringUtil;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import com.idear.framework.template.BusinessTemplate;
import com.idear.sms.core.enums.SmsTemplateStatus;
import com.idear.sms.core.mapper.SmsTemplateMapper;
import com.idear.sms.core.po.SmsTemplate;
import com.idear.sms.service.dto.TemplateDto;
import com.idear.sms.service.facade.SmsTemplateService;
import com.idear.sms.service.message.SmsMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
@Service("smsTemplateService")
public class SmsTemplateServiceImpl implements SmsTemplateService {

    @Resource
    private SmsTemplateMapper smsTemplateMapper;

    @Resource
    private BusinessTemplate businessTemplate;


    /**
     * 根据位置信息获取banner信息
     *
     * @return
     */
    @Override
    public Response<List<TemplateDto>> listTemplate() {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                List<SmsTemplate> templateList = smsTemplateMapper.listTemplate();
                return new Response(convert2DtoList(templateList));
            }
        });
    }

    /**
     * 添加广告信息
     *
     * @param dto
     * @return
     */
    @Override
    public Response<Integer> addTemplate(final TemplateDto dto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                SmsTemplate template = new SmsTemplate();
                //默认图片类型
                template.setCode(dto.getCode());
                template.setContent(dto.getContent());
                Date nowDate = new Date();
                template.setCreateTime(nowDate);
                template.setUpdateTime(nowDate);
                template.setStatus(SmsTemplateStatus.ONLINE.getStatus());
                smsTemplateMapper.saveTemplate(template);
                return new Response(template.getId());
            }
        });
    }

    /**
     * 删除Banner信息
     *
     * @param templateId
     * @return
     */
    @Override
    public Response deleteTemplate(final Integer templateId) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                SmsTemplate template = smsTemplateMapper.getTemplate(templateId);
                if (template == null){
                    throw new BusinessException(SmsMessage.SMS_TEMPLATE_NOT_EXIST);
                }
                template.setStatus(SmsTemplateStatus.OFFLINE.getStatus());
                smsTemplateMapper.updateTemplate(template);
                return Response.SUCCESS;
            }
        });
    }

    /**
     * 更新banner信息
     *
     * @param dto
     * @return
     */
    @Override
    public Response updateTemplate(final TemplateDto dto) {
        return businessTemplate.execute(new Action() {
            @Override
            public Response execute() {
                Integer templateId = dto.getId();
                SmsTemplate template = smsTemplateMapper.getTemplate(templateId);
                if (template == null){
                    throw new BusinessException(SmsMessage.SMS_TEMPLATE_NOT_EXIST);
                }
                if (StringUtil.isNotEmpty(dto.getCode())){
                    template.setCode(dto.getCode());
                }
                if (StringUtil.isNotEmpty(dto.getContent())){
                    template.setContent(dto.getContent());
                }
                if (dto.getStatus() != null){
                    template.setStatus(dto.getStatus());
                }
                smsTemplateMapper.updateTemplate(template);
                return Response.SUCCESS;
            }
        });
    }

    private TemplateDto convert2Dto(SmsTemplate template) {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId(template.getId());
        templateDto.setCode(template.getCode());
        templateDto.setContent(template.getContent());
        templateDto.setStatus(template.getStatus());
        templateDto.setCreateTime(template.getCreateTime());
        templateDto.setUpdateTime(template.getUpdateTime());
        return templateDto;
    }

    private List<TemplateDto> convert2DtoList(List<SmsTemplate> templateList){
        List<TemplateDto> dtoList = new ArrayList<>();
        if (templateList != null && !templateList.isEmpty()) {
            Iterator<SmsTemplate> iterator = templateList.iterator();
            while (iterator.hasNext()) {
                SmsTemplate template = iterator.next();
                dtoList.add(convert2Dto(template));
            }
        }
        return dtoList;
    }
}
