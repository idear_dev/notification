/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 梦网科技配置信息
 *
 * @author CcShan
 * @date 2017/12/15
 */
@Component
public class MontnetsConfig {

    @Value("${montnets.userId}")
    private String userId;

    @Value("${montnets.password}")
    private String password;

    @Value("${montnets.address}")
    private String address;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSingleSendUrl() {
        return getAddress() + SINGLE_SEND_UR;
    }

    private static final String SINGLE_SEND_UR = "/sms/v2/std/single_send";
}
