/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.helper;

import com.alibaba.fastjson.JSONObject;
import com.idear.common.constants.MapConstant;
import com.idear.common.exception.impl.BusinessException;
import com.idear.common.util.HttpUtil;
import com.idear.common.util.JsonUtil;
import com.idear.common.util.Md5Util;
import com.idear.common.util.SerialNoUtil;
import com.idear.sms.config.MontnetsConfig;
import com.idear.sms.core.enums.SmsHistoryStatus;
import com.idear.sms.core.mapper.SmsHistoryMapper;
import com.idear.sms.core.po.SmsHistory;
import com.idear.sms.core.po.SmsTemplate;
import com.idear.sms.service.message.SmsMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author CcShan
 * @date 2017/12/15
 */
@Component
public class SendMessageHelper {

    @Autowired
    private MontnetsConfig montnetsConfig;

    @Autowired
    private SmsHistoryMapper smsHistoryMapper;

    /**
     * 发送单条短信
     * @param mobileNo
     * @param smsTemplate
     * @param params
     */
    public void sendSingle(String mobileNo,SmsTemplate smsTemplate, String[] params){
        String templateContent = smsTemplate.getContent();
        String message = MessageFormat.format(templateContent,params);
        String serialNo = SerialNoUtil.generateSerialNoWithUUID();
        Map requestMap = prepareRequestMap(serialNo,mobileNo,message);
        String requestUrl = montnetsConfig.getSingleSendUrl();
        String thdSerialNo = null;
        SmsHistoryStatus smsHistoryStatus = SmsHistoryStatus.SUCCEED;
        try {
            String responseBody = HttpUtil.postJson(requestUrl,null, JsonUtil.toJsonString(requestMap));
            thdSerialNo = getThdSerialNo(responseBody);
        }catch (Exception e){
            smsHistoryStatus = SmsHistoryStatus.FAILED;
            throw e;
        }finally {
            Date nowDate = new Date();
            SmsHistory smsHistory = new SmsHistory();
            smsHistory.setSerialNo(serialNo);
            smsHistory.setMobileNo(mobileNo);
            smsHistory.setTemplateCode(smsTemplate.getCode());
            smsHistory.setContent(message);
            smsHistory.setThdSerialNo(thdSerialNo);
            smsHistory.setStatus(smsHistoryStatus.getStatus());
            smsHistory.setCreateTime(nowDate);
            smsHistory.setUpdateTime(nowDate);
            smsHistoryMapper.saveHistory(smsHistory);
        }


    }

    private String getThdSerialNo(String responseBody){
        JSONObject jsonObject = JsonUtil.parseJsonObject(responseBody);
        if (jsonObject.getInteger("result").equals(0)){
            return jsonObject.getString("msgid");
        }else {
            throw new BusinessException(SmsMessage.THD_SEND_MESSAGE_FAILED);
        }
    }
    /**
     * 准备请求参数
     * @param serialNo
     * @param mobileNo
     * @param message
     * @return
     */
    public Map<String,String> prepareRequestMap(String serialNo,String mobileNo,String message){
        Map<String,String> requestMap = new HashMap<>(MapConstant.INITIALCAPACITY_16);
        Date nowDate = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddHHmmss");
        String timestamp = simpleDateFormat.format(nowDate);
        requestMap.put("userid",montnetsConfig.getUserId());
        requestMap.put("pwd",generatePassword(timestamp));
        requestMap.put("mobile",mobileNo);
        requestMap.put("timestamp",timestamp);
        requestMap.put("custid",serialNo);
        try {
            requestMap.put("content", URLEncoder.encode(message,"GBK"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return requestMap;
    }

    private String generatePassword(String timestamp){
        String plain = montnetsConfig.getUserId()+"00000000"+montnetsConfig.getPassword()+timestamp;
        return Md5Util.md5(plain);
    }
}
