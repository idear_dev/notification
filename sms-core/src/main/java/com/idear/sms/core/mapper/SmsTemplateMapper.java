/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.core.mapper;

import com.idear.sms.core.po.SmsTemplate;

import java.util.List;

/**
 *
 * @author CcShan
 * @date 2017/12/14
 */
public interface SmsTemplateMapper {

    /**
     * 获取短信模板
     * @param code
     * @return
     */
    SmsTemplate getSmsTemplateByCode(String code);

    /**
     * 短信模版信息
     * @return
     */
    List<SmsTemplate> listTemplate();

    /**
     * 保存短信模版
     * @param template
     */
    void saveTemplate(SmsTemplate template);

    /**
     * 更新template信息
     * @param template
     */
    void updateTemplate(SmsTemplate template);

    /**
     * 查询banner详情
     * @param id
     * @return
     */
    SmsTemplate getTemplate(Integer id);
}
