/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.core.enums;

/**
 *
 * @author CcShan
 * @date 2017/12/14
 */
public enum SmsTemplateStatus {

    /**
     * 在线
     */
    ONLINE(1,"在线"),

    /**
     * 下线
     */
    OFFLINE(9,"下线");

    private Integer status;

    private String desc;

    SmsTemplateStatus(Integer status,String desc){
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
