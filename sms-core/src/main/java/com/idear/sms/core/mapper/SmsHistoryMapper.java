/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.core.mapper;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.idear.sms.core.po.SmsHistory;

import java.util.Map;

/**
 * @author CcShan
 * @date 2017/12/14
 */
public interface SmsHistoryMapper {

    /**
     * 保存发送记录
     *
     * @param smsHistory
     */
    void saveHistory(SmsHistory smsHistory);

    /**
     * 分页查询短信发送记录
     * @param param
     * @param pageBounds
     * @return
     */
    PageList<SmsHistory> listHistory(Map<String, Object> param, PageBounds pageBounds);
}
