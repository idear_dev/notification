/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.core.enums;

/**
 *
 * @author CcShan
 * @date 2017/12/14
 */
public enum SmsHistoryStatus {

    /**
     * 短信发送成功
     */
    SUCCEED(1,"发送成功"),

    /**
     * 短信发送失败
     */
    FAILED(9,"发送失败");

    private Integer status;

    private String desc;

    SmsHistoryStatus(Integer status,String desc){
        this.status = status;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDesc() {
        return desc;
    }
}
