/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.core.constants;

/**
 *
 * @author CcShan
 * @date 2017/12/15
 */
public class CacheConstant {

    public static final String SMS_TEMPLATE_INDEX = "SMS:TEMPLATE_";
}
