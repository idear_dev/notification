/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.sms.service.facade;

import com.idear.framework.response.Response;
import com.idear.sms.service.dto.TemplateDto;

import java.util.List;

/**
 * @author ShanCc
 * @date 2018/11/1
 */
public interface SmsTemplateService {

    /**
     * 根据位置信息获取banner信息
     * @return
     */
    Response<List<TemplateDto>> listTemplate();

    /**
     * 添加广告信息
     * @param dto
     * @return
     */
    Response<Integer> addTemplate(TemplateDto dto);

    /**
     * 删除Banner信息
     * @param templateId
     * @return
     */
    Response deleteTemplate(Integer templateId);

    /**
     * 更新banner信息
     * @param dto
     * @return
     */
    Response updateTemplate(TemplateDto dto);
}
