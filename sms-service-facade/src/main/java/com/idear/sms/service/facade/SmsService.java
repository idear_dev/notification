/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.service.facade;

import com.idear.framework.response.Response;
import com.idear.sms.service.dto.HistoryListDto;
import com.idear.sms.service.param.QueryHistoryParam;
import com.idear.sms.service.param.SendSingleMessageParam;


/**
 *
 * @author CcShan
 * @date 2017/12/14
 */
public interface SmsService {

    /**
     * 发送单条短消息
     * @param sendMessageParam
     * @return
     */
    Response sendSingleMessage(SendSingleMessageParam sendMessageParam);

    /**
     * 查询短信发送记录
     * @param param
     * @return
     */
    Response<HistoryListDto> queryHistory(QueryHistoryParam param);
}
