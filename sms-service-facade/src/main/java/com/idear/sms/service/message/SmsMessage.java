/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.service.message;

import com.idear.common.message.Message;

/**
 *
 * @author CcShan
 * @date 2017/12/15
 */
public enum SmsMessage implements Message{

    /**
     * SMS错误码区间SMS(8000～8200]
     */
    SMS_TEMPLATE_NOT_EXIST("8001","短信模板不存在"),
    THD_SEND_MESSAGE_FAILED("8002","第三方发送短信失败");
    private String respCode;

    private String respMsg;

    SmsMessage(String respCode,String respMsg){
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    @Override
    public String getRespCode() {
        return respCode;
    }

    @Override
    public String getRespMsg() {
        return respMsg;
    }
}
