/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.sms.service.param;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * @author CcShan
 * @date 2017/12/15
 */
public class SendSingleMessageParam implements Serializable{

    /**
     * 模版编码
     */
    @NotEmpty
    private String templateCode;

    /**
     * 手机号
     */
    @NotEmpty
    private String mobileNo;

    /**
     * 参数数组
     */
    @NotNull
    private String[] params;

    /**
     * 延迟发送时间（秒）
     */
    private Integer delayTimes = 0;

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String[] getParams() {
        return params;
    }

    public void setParams(String[] params) {
        this.params = params;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Integer getDelayTimes() {
        return delayTimes;
    }
}
